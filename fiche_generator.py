#! /usr/bin/env python3

import os
import sys
import jinja2
import yaml
from datetime import date

texenv = jinja2.Environment(
    block_start_string='%{',
    block_end_string='%}',
    variable_start_string='%{{',
    variable_end_string='%}}',
    comment_start_string='{{#',
    comment_end_string='#}}',
    loader=jinja2.FileSystemLoader(os.path.abspath('.')))


def gen_fiche(infile, outfile):
    # Load the conference details
    with open(infile) as fiche_file:
        fiche = yaml.load(fiche_file)

    # Load the conference's responsible member details
    with open(os.path.join('ressources', fiche['resp'] + '.yml')) as resp:
        fiche['resp'] = yaml.load(resp)

    # Load the conference's "tuteur" details
    if fiche['tuteur']:
        with open(os.path.join('ressources', fiche['tuteur'] + '.yml')) as tut:
            fiche['tuteur'] = yaml.load(tut)

    # Load the GConfs' president details
    with open(os.path.join('ressources', fiche['president'] + '.yml')) as pres:
        fiche['president'] = yaml.load(pres)

    # Get today's date
    fiche['today'] = date.today()

    template = texenv.get_template(os.path.join('ressources', 'template.tex'))

    fiche = template.render(fiche)

    with open(outfile, 'w+') as out:
        out.write(fiche)

if __name__ == "__main__":
    infile = sys.argv[1]
    if len(sys.argv) >= 3:
        outfile = sys.argv[2]
    else:
        outfile = os.path.splitext(os.path.basename(infile))[0] + '.tex'
    gen_fiche(infile, outfile)
