`./fiche_generator.py infile.yml [outfile.tex]`

Takes a yaml file (see an exemple in exemple folder) and generate a tex file
which you can use with pdflatex.

See the example folder for yaml syntax.
